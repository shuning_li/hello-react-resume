import React, { Component } from 'react';
import Header from './components/Header';
import About from './components/About';
import Education from './components/Education';
import './App.less';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      age: -1,
      description: '',
      educations: []
    };
  }

  componentDidMount() {
    fetch('http://localhost:3000/person')
      .then(res => res.json())
      .then(res => {
        const { name, age, description, educations } = res;
        this.setState({
          name,
          age,
          description,
          educations
        });
      });
  }

  render() {
    const { name, age, description, educations } = this.state;
    return (
      <main className="app">
        <Header name={name} age={age} />
        <About description={description} />
        <Education educations={educations} />
      </main>
    );
  }
}

export default App;
