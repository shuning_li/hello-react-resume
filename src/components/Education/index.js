import React from 'react';
import PropTypes from 'prop-types';
import './index.less';

const Education = props => {
  const { educations } = props;
  return (
    <section className="education">
      <h3>EDUCATION</h3>
      <section className="education-main">
        <ul>
          {educations.map(item => (
            <li key={item.year}>
              <section className="education-year">{item.year}</section>
              <section className="education-content">
                <section className="education-title">{item.title}</section>
                <section className="education-description">
                  {item.description}
                </section>
              </section>
            </li>
          ))}
        </ul>
      </section>
    </section>
  );
};

Education.propTypes = {
  educations: PropTypes.array
};

export default Education;
