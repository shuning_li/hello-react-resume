import React from 'react';
import PropTypes from 'prop-types';
import './index.less';

const About = props => {
  const { description } = props;
  return (
    <section className="about">
      <h3 className="title">ABOUT ME</h3>
      <section className="about-content">{description}</section>
    </section>
  );
};

export default About;

About.propTypes = {
  description: PropTypes.string
};
