import React from 'react';
import PropTypes from 'prop-types';
import './index.less';
import avatar from '../../assets/avatar.jpg';

const Header = props => {
  const { name, age } = props;
  return (
    <header className="resume-header">
      <section className="header-content">
        <img className="avatar" src={avatar} alt="" />
        <section className="introduce">
          <h1>Hello,</h1>
          <h2>
            MY NAME IS {name} {age}YO AND THIS IS MY RESUME/CV
          </h2>
        </section>
      </section>
    </header>
  );
};

Header.propTypes = {
  name: PropTypes.string,
  age: PropTypes.number
};

export default Header;
